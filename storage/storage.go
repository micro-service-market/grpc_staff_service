package storage

import (
	"context"
	"staff/genproto/staff_service"
)

type StoregeI interface {
	Staff() StaffsI
	StaffTarif() StaffTarifI
}

type StaffsI interface {
	CreateStaff(context.Context, *staff_service.CreateStaff) (string, error)
	UpdateStaff(context.Context, *staff_service.Staff) (string, error)
	GetStaff(context.Context, *staff_service.IdRequest) (*staff_service.Staff, error)
	GetAllStaff(context.Context, *staff_service.GetAllStaffRequest) (*staff_service.GetAllStaffResponse, error)
	DeleteStaff(context.Context, *staff_service.IdRequest) (string, error)
	UpdateBalance(context.Context, *staff_service.UpdateBalanceRequest) (string, error)
	GetByUsername(context.Context, *staff_service.RequestByUsername) (*staff_service.Staff, error)
	ChangePassword(context.Context, *staff_service.RequestByPassword) (string, error)
	/* ChangeBalance(id string, amount float64) (string, error)
	GetMapOfStaffs() (map[string]models.Staff, error) */
}

type StaffTarifI interface {
	CreateStaffTarif(context.Context, *staff_service.CreateTarif) (string, error)
	UpdateStaffTarif(context.Context, *staff_service.Tarif) (string, error)
	GetStaffTarif(context.Context, *staff_service.IdRequest) (*staff_service.Tarif, error)
	GetAllStaffTarif(context.Context, *staff_service.GetAllTarifRequest) (*staff_service.GetAllTarifResponse, error)
	DeleteStaffTarif(context.Context, *staff_service.IdRequest) (string, error)
}
