package grpc_client

import (
	"fmt"

	"staff/config"
	"staff/genproto/branch_service"
	"staff/genproto/catalog_service"
	"staff/genproto/sale_service"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

// GrpcClientI ...
type GrpcClientI interface {
	BranchService() branch_service.BranchServiceClient
	SaleService() sale_service.SaleServerClient
	SaleProductService() sale_service.SaleProductServerClient
	BranchTransactionService() sale_service.BranchTransactionServerClient
	TransactionService() sale_service.TransactionServerClient
	// CatagoryService() catalog_service.CategoryServiceClient
	// ProductService() catalog_service.ProductServiceClient
}

// GrpcClient ...
type GrpcClient struct {
	cfg         config.Config
	connections map[string]interface{}
}

// New ...
func New(cfg config.Config) (*GrpcClient, error) {
	connSale, err := grpc.Dial(fmt.Sprintf("%s:%d", cfg.SaleServiceHost, cfg.SaleServisePort), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, fmt.Errorf("sale service dial host: %s port:%d err: %s",
			cfg.SaleServiceHost, cfg.SaleServisePort, err)
	}

	connBranch, err := grpc.Dial(fmt.Sprintf("%s:%d", cfg.BranchServiceHost, cfg.BranchServisePort), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, fmt.Errorf("courier service dial host: %s port:%d err: %s",
			cfg.BranchServiceHost, cfg.BranchServisePort, err)
	}

	connCatalog, err := grpc.Dial(fmt.Sprintf("%s:%d", cfg.CatalogServiceHost, cfg.CatalogServisePort), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, fmt.Errorf("courier service dial host: %s port:%d err: %s",
			cfg.CatalogServiceHost, cfg.CatalogServisePort, err)
	}

	return &GrpcClient{
		cfg: cfg,
		connections: map[string]interface{}{
			"branch_service":             branch_service.NewBranchServiceClient(connBranch),
			"sale_service":               sale_service.NewSaleServerClient(connSale),
			"sale_product_service":       sale_service.NewSaleProductServerClient(connSale),
			"branch_transaction_service": sale_service.NewBranchTransactionServerClient(connSale),
			"transaction_service":        sale_service.NewTransactionServerClient(connSale),
			"category_service":           catalog_service.NewCategoryServiceClient(connCatalog),
			"product_service":            catalog_service.NewProductServiceClient(connCatalog),
		},
	}, nil
}

func (g *GrpcClient) BranchService() branch_service.BranchServiceClient {
	return g.connections["branch_service"].(branch_service.BranchServiceClient)
}

func (g *GrpcClient) SaleService() sale_service.SaleServerClient {
	return g.connections["sale_service"].(sale_service.SaleServerClient)
}

func (g *GrpcClient) SaleProductService() sale_service.SaleProductServerClient {
	return g.connections["sale_product_service"].(sale_service.SaleProductServerClient)
}

func (g *GrpcClient) BranchTransactionService() sale_service.BranchTransactionServerClient {
	return g.connections["branch_transaction_service"].(sale_service.BranchTransactionServerClient)
}

func (g *GrpcClient) TransactionService() sale_service.TransactionServerClient {
	return g.connections["transaction_service"].(sale_service.TransactionServerClient)
}

// func (g *GrpcClient) CategoryService() catalog_service.CategoryServiceClient {
// 	return g.connections["category_service"].(catalog_service.CategoryServiceClient)
// }

// func (g *GrpcClient) ProductService() catalog_service.ProductServiceClient {
// 	return g.connections["product_service"].(catalog_service.ProductServiceClient)
// }
